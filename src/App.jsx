import { useState } from "react";

function App() {

  let [dice,setDice] = useState({dice1:1,dice2:1})

  let winner = dice.dice1 > dice.dice2 ? "Player 1 " : dice.dice1 < dice.dice2 ? "player2 " : "tie"
  
  function diceRotate() {
    
    let newDice = {...dice}
    newDice.dice1 = Math.floor(Math.random() * 6) + 1
    newDice.dice2 = Math.floor(Math.random() * 6) + 1
    setDice(newDice)

  }

  return (
    <div>
      
      <div className="bg-black w-[50%] m-auto mt-24 rounded-lg">

        <h1 className="text-white font-extrabold text-center py-5">Winner : {winner}</h1>

        <div className="flex justify-center items-center ml-[15%]">

          <div>
            <h1 className="text-white font-extrabold mr-[35%] text-center">Player 1</h1>
            <img src={`dice${dice.dice1}.png`} className="w-[60%] h-[60%] transform transition duration-500 hover:rotate-90" alt={dice.dice1} />
          </div>
          <div>
            <h1 className="text-white  font-extrabold mr-[35%] text-center">Player 2</h1>
            <img src={`dice${dice.dice2}.png`} className="w-[60%] h-[60%] transform transition duration-500 hover:rotate-90" alt={dice.dice2} />

          </div>

        </div>
        <button onClick={diceRotate} className="bg-red-500 px-10 text-black my-10 py-3 rounded-lg sm:mx-[43%] ml-14">Play</button>
      </div>
      
    </div>
  );
}

export default App;
